# Sweet Corn Cake
Two days ago I decided I would bake a cornmeal cake, known in Brazil as Bolo de Fubá. [Fubá](https://www.google.com/search?biw=1791&bih=836&tbm=isch&ei=UGFHXJ69CoL4wAKIvYLYBg&q=fub%C3%A1&oq=fub%C3%A1&gs_l=img.3..0l2j0i5i30l2j0i30l6.6101.7359..7756...1.0..0.57.314.6......1....1..gws-wiz-img.......0i10i30j0i5i10i30.l7AlCtk59pE) is widely used to make cakes, couscouz, and bread. Think of great childhood memories, harvest festival in June, and a warm cup of coffee -- you can pair all those things up with a warm and sweet piece of corn cake. But here's the thing: I wasn't really in the mood for baking. I thought I was, but I wasn't. The result you might guess: a total failure. I used different ingredients from what I am used to (hint: avoid bottled egg whites)... and the batter looked odd which I already knew that would into the driest cake ever baked. Hmm... nothing felt right. I then transfered some of the batter into cupcake tray, hoping I would get something good out of it. Nope. Failure. A complete waste of food and energy.

I guess I had one of those enlightened moments that sometimes cooking provides: Not everything will turned out perfect, and it's fine to not push yourself when you don't really feel like it. Give yourself some time to rest and try again later.

And so I did. Last night I looked up some corn cake recipes from two of my favorite Brazilian cookbooks and decided to try again. I already knew from previous baking that one of the recipes offered a dry-textured cake while the other one would result into something soufflé-like. Being as stubbobrn as a cook can be, I decided to mix those two recipes up and follow my gut feeling. And boy, was I right! The end product is a combination of both worlds, and I loved it.

## Ingredients
- 68g of sifted all purpose flour
- 240g of yellow cornmeal
- 270g of sugar
- 4 eggs, separated
- 280ml of milk
- 200g of unsalted butter
- 2 tbsp of baking powder

## Directions
- Preheat the oven to 180°C. Grease a round mould with a hole in the middle and dust with sifted all purpose flour.
- In a large bowl, whist the egg whites until frothy using an electric hand mixer. Set aside.
- Combine all the dry ingredients, except for the baking powder. Set aside.
- Beat together the butter and egg yolks until light and fluffy. Stir in the milk, then add it to the dry ingredients. Lastly, add the baking powder. Beat until smooth.
- Fold the whisked egg whites into the batter using slow movements until well incorporated.
- Pour the batter into the greased mould and bake it for about 30 minutes or until is baked through.
